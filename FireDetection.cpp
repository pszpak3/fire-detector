#include "FireDetection.h"

FireDetection::FireDetection(int low_H, int low_S, int low_V, int high_H, int high_S, int high_V)
{
	this->low_H = low_H;
	this->low_S = low_S;
	this->low_V = low_V;

	this->high_H = high_H;
	this->high_S = high_S;
	this->high_V = high_V;

	iterator = 0;

}
FireDetection::~FireDetection() {}

void FireDetection::processAlgorithm(String videoPath)
{
	Mat frame, frameThreshold;
	int key;
	vector<vector<Point> > contours;

	VideoCapture cap = initializeVideo(videoPath);

    while (1) {
		//czytaj klatke
		bool frameReadSuccess = cap.read(frame);
		if (!frameReadSuccess) {
			cout << "Error: Cannot read a frame from video file" << endl;
		}

		getFrameThreshold(frame).copyTo(frameThreshold);
		contours = getSortedFireCountours(frameThreshold);
		drawFireContour(frame, contours);

		//wyswietl
        imshow(window_capture_name, frame);
		imshow(window_detection_name, frameThreshold);

		key = waitKey(20);
		if (videoPath == "camera") {
			if ((key == 27))
			{
				destroyAllWindows();
				cap.release();
				break;
			}

		}
		else
		{
			if ((cap.get(CAP_PROP_POS_FRAMES) == cap.get(CAP_PROP_FRAME_COUNT)) || (key == 27))
			{
				destroyAllWindows();
				cap.release();
				break;
			}
		}

		
    }
}


VideoCapture FireDetection::initializeVideo(String videoPath)
{
	if (videoPath == "camera") {
		VideoCapture cap(0);
		if (!cap.isOpened()) {
			cout << "Przechwytywanie obrazu z kamery zakonczylo sie niepowodzeniem" << endl;
		}
		return cap;
	}
	else
	{
		VideoCapture cap(videoPath);
		if (!cap.isOpened()) {
			cout << "Przechwytywanie obrazu video zakonczylo sie niepowodzeniem" << endl;
		}
		return cap;
	}
}


Mat FireDetection::getFrameThreshold(Mat frame)
{
	Mat frame_HSV, frame_threshold;

	//konwersja na model HSV
	cvtColor(frame, frame_HSV, COLOR_BGR2HSV);

	// binaryzacja metod� inRange
	inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(high_H, high_S, high_V), frame_threshold);

	//erozja x2, dylatacja x2
	erode(frame_threshold, frame_threshold, getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)), Point(-1, -1), 2);
	dilate(frame_threshold, frame_threshold, getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)), Point(-1, -1), 2);

	return frame_threshold;
}

vector<vector<Point>> FireDetection::getSortedFireCountours(Mat frameThreshold)
{
	//wektory konturow
	vector<vector<Point> > contours;
	//hierarchia konturow
	vector<Vec4i> hierarchy;

	//znalezienie konturow ognia 
	findContours(frameThreshold, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0));

	

	//sortowanie konturow
	sort(contours.begin(), contours.end(), [](vector<Point> contour1, vector<Point> contour2) {
			double i = fabs(contourArea(Mat(contour1)));
			double j = fabs(contourArea(Mat(contour2)));
			return (i < j);
	});


	return contours;
}

void FireDetection::drawFireContour(Mat frame, vector<vector<Point>> contours)
{
	Rect boundRect;
	if (contours.size() > 0) {
		vector<Point> biggestContour = contours[contours.size() - 1];

		if (iterator == 20 || iterator == 0) {
			PlaySound(TEXT("alarm.WAV"), NULL, SND_ASYNC);
			iterator = 0;
		}
		iterator++;

		boundRect = boundingRect(biggestContour);

		rectangle(frame, boundRect, Scalar(0, 255, 0), 2);
		putText(frame, "Fire Detected", Point(30, 30), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(43, 255, 0), 2);
	}
}


