#pragma once
#pragma comment(lib, "winmm.lib")

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "windows.h"
#include "mmsystem.h"
#include <iostream>
#include <conio.h>

using namespace cv;
using namespace std;

class FireDetection
{
private:
	const int max_value_H = 360 / 2;
	const int max_value = 255;
	const String window_capture_name = "Video Capture";
	const String window_detection_name = "Fire Detection";
	int low_H;
	int	low_S;
	int low_V;

	int high_H;
	int high_S;
	int high_V;

	int iterator;

public:
	FireDetection(int low_H, int low_S, int low_V, int high_H, int high_S, int high_V);
	~FireDetection();
	void processAlgorithm(String videoPath);
	VideoCapture initializeVideo(String videoPath);
	Mat getFrameThreshold(Mat frame);
	vector<vector<Point>> getSortedFireCountours(Mat frameThreshold);
	void drawFireContour(Mat frame, vector<vector<Point>> contours);
};

