#include <iostream>
#include <conio.h>
#include "FireDetection.h"
//#include "zad_01_kamerka.cpp"
//#include "zad_01_bike_and_robot.cpp"


using namespace std;


int main() {
	FireDetection* fireDetection = new FireDetection(0,0,120,50,80,255);
	//FireDetection* fireDetection = new FireDetection(0, 74, 200, 18, 166, 230);


	int option;
	cout << "Wybierz obraz:" << endl;
	cout << "1 - Film 'fire at lake'" << endl;
	cout << "2 - Film 'big fire'" << endl;
	cout << "3 - Film 'gun fire'" << endl;
	cout << "4 - Film 'campfire'" << endl;
	cout << "5 - Camera" << endl;


	cout << "Opcja: ";
	cin >> option;

	switch (option)
	{
	case 1:
		fireDetection->processAlgorithm("fireAtLake.mp4");
		break;
	case 2:
		fireDetection->processAlgorithm("bigFire.mp4");
		break;
	case 3:
		fireDetection->processAlgorithm("gunFire.mp4");
		break;
	case 4:
		fireDetection->processAlgorithm("campFire.mp4");
		break;
	case 5:
		fireDetection->processAlgorithm("camera");
		break;


	default:
		break;
	}
}